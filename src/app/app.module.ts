import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabPageComponent } from './tab-page/tab-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { MaterialModule } from './angular-material.module';
import { TabOneComponent } from './tab-page/tab-one/tab-one.component';
import { TabTwoComponent } from './tab-page/tab-two/tab-two.component';
import { FormComponent } from './tab-page/tab-one/form/form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SearchLibModule } from './search-lib/search-lib.module';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './services/in-memory-data.service';
import { HttpClientModule } from '@angular/common/http';
import { Tab2FormComponent } from './tab-page/tab-two/form/form.component';
import { CommentDialogComponent } from './tab-page/tab-one/comment-dialog/comment-dialog.component';
import { ViewAndCompareDialogComponent } from './tab-page/tab-two/view-and-compare-dialog/view-and-compare-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    TabPageComponent,
    SearchPageComponent,
    TabOneComponent,
    TabTwoComponent,
    FormComponent,
    Tab2FormComponent,
    CommentDialogComponent,
    ViewAndCompareDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    MaterialModule,
    SearchLibModule
  ],
  entryComponents: [FormComponent, Tab2FormComponent, CommentDialogComponent, ViewAndCompareDialogComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
