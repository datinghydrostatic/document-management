import { Injectable } from '@angular/core';
import { ClientSearch, ClientDocument, MembershipGrouping } from '../model';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  searchResultSelectedRow: ClientSearch = null;
  tab1SelectedRow: ClientDocument = null;
  tab2SelectedRow: MembershipGrouping = null;
  onSearchResultRowSelect: Subject<any> = new BehaviorSubject<any>('');
  onTab1RowSelect: Subject<any> = new BehaviorSubject<any>('');
  constructor() { }
}
