import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ClientDocument, MembershipGrouping } from '../model';
import { MatSnackBar } from '@angular/material';


@Injectable({ providedIn: 'root' })
export class ApiService {

  tab1Url = 'api/tab1Data';
  tab2Url = 'api/tab2Data';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient, public snackBar: MatSnackBar) { }

  /** GET data from the server */

  getTab1Data(clientId: number) {
    // this.tab1Url + '/' + clientId
    return this.http.get<ClientDocument>(this.tab1Url, {})
      .pipe(
        catchError(this.handleError.bind(this)) // then handle the error
      );
  }

  getTab2Data(contractId: number) {
    // this.tab1Url + '/' + contractId
    return this.http.get<MembershipGrouping>(this.tab2Url, {})
      .pipe(
        catchError(this.handleError.bind(this)) // then handle the error
      );
  }

  //////// Save methods //////////

  /** POST: add a new rec to the server */
  addTab1Row(row: ClientDocument): Observable<ClientDocument> {
    return this.http.post<ClientDocument>(this.tab1Url, row, this.httpOptions).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  addTab2Row(row: MembershipGrouping): Observable<MembershipGrouping> {
    return this.http.post<MembershipGrouping>(this.tab2Url, row, this.httpOptions).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  addComment(comment: string): Observable<ClientDocument> {
    return this.http.post<any>(this.tab1Url, { comment }, this.httpOptions).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  /** DELETE: delete the row from the server */
  deleteTab1Row(row: ClientDocument | number): Observable<ClientDocument> {
    const id = typeof row === 'number' ? row : row.contractId;
    const url = `${this.tab1Url}/${id}`;

    return this.http.delete<ClientDocument>(url, this.httpOptions).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  deleteTab2Row(row: MembershipGrouping | number): Observable<MembershipGrouping> {
    const id = typeof row === 'number' ? row : row.membershipGroupingId;
    const url = `${this.tab2Url}/${id}`;

    return this.http.delete<MembershipGrouping>(url, this.httpOptions).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  /** PUT: update the TabRow on the server */
  updateTab1Row(row: ClientDocument): Observable<any> {
    return this.http.put(this.tab1Url, row, this.httpOptions).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  updateTab2Row(row: MembershipGrouping): Observable<any> {
    return this.http.put(this.tab1Url, row, this.httpOptions).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  updateTab2Rows(updatedRowsData: any[]): Observable<any> {
    return this.http.put(this.tab1Url, updatedRowsData, this.httpOptions).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
    } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
    }
    // this.isLoading = false;
    this.snackBar.open('Failed.' + error.status + error.message, 'Clear',
        {
            duration: 152000,
            horizontalPosition: 'end',
            verticalPosition: 'top',
            panelClass: ['red-snackbar']
        });
    // return an observable with a user-facing error message
    return throwError(
        'Something bad happened; please try again later.');
}

}


