import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabPageComponent } from './tab-page/tab-page.component';
import { SearchPageComponent } from './search-page/search-page.component';


const routes: Routes = [
  { path: '', component: SearchPageComponent },
    { path: 'tabpage', component: TabPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
