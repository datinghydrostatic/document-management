// Model for search page

export interface ClientSearch {
    clientId: number;
    contractId: number;
    accountCommonName: string;
    billingStatus: string;
    policyCommonName: string;
    effectiveDate: string;
    endDate: string;
}

// Model for Tab 1 page
export interface ClientDocument {
    clientId: number;
    contractId: number;
    salesPolicy: SalesPolicy;
    renewalLetterAdvanceMonths: number;
    renewalReportDate: string;
    delayPaymentPeriodForBenefit: number;
    limitBenefitByFutureTermDate: string;
    status: string;
    billingStatus: string;
    effectiveBillingStatus: string;
    billingPendedIndicator: boolean;
    eligibilityStatus: string;
    nextAnniversaryDate: string;
    currentAnniversaryDate: string;
    effectiveDateCreateDate: string;
    specialHandlingCodes?: (SpecialHandlingCodesEntity)[] | null;
    salesAccountCommonName: string;
}

export interface SalesPolicy {
    originalEffectiveDate: string;
    effectiveDate: string;
    clientType: string;
    clientTypeCalculated: string;
    legalName: string;
    policyCommonName: string;
    contractState: string;
    flexOptions: string;
    policyPeriodInMonths: number;
    sapTargetDate: string;
    soldRatesContainRates: boolean;
    leadRenewalRepresentative: string;
    leadServiceRepresentative: string;
    revenueRepresentativeName: string;
    flexEmpContDep: number;
    flexEmpContEe: number;
    gsaId: string;
    earliestEffectiveDate: string;
    address: Address;
}

export interface Address {
    street1: string;
    city: string;
    stateCode: string;
    zipCode: string;
    zipExtension: string;
    zipStandard: string;
}

export interface SpecialHandlingCodesEntity {
    codeType: string;
}


// Model for Tab 2
export interface MembershipGrouping {
    clientId: number;
    contractId: number;
    membershipGroupingId: string;
    manualUpdateIndicator: string;
    manualUpdateIndicatorParsed: boolean;
    flexValidCode: string;
    flexValidCodeRaw: string;
    customerServiceLocation: string;
    rejectMethodCode: string;
    populationIndicator: string;
    dependentTrackingIndicatorParsed: boolean;
    dependentTrackingExtendedCalculated: boolean;
    addressTrackingIndicatorParsed: boolean;
    originalEffectiveDate: Date;
    effectiveDate: Date;
    billingGroupingId: string;
    statementsPerYear: number;
    statementType: string;
    statementFeeCode: string;
    onlineBillingOptions: boolean;
    sapBillPrintCode: string;
    billingGroupingName: string;
    billingGroupingName1Raw: string;
    billingGroupingAddress: BillingGroupingAddress;
    billingContractStateCode: string;
    altPayIdInd: boolean;
    sapBilling: boolean;
    sapBillingRaw: string;
    individuallyBilled: boolean;
    membershipGroupingSalesData: {};
    effDateCreateDate: Date;
    revenueAdminRepId: string;
    certificationMethod: string;
    status: string;
    eligibilityStatus: string;
    billingStatus: string;
    effectiveBillingStatus: string;
    billingPendedIndicator: boolean;
    pmpmDependentCountIndicator: string;
    pmpmDependentCountIndicatorParsed: boolean;
    earliestEffectiveDate: Date;
}

export interface BillingGroupingAddress {
    street1: string;
    city: string;
    stateCode: string;
    zipCode: string;
    zipExtension: string;
    zipStandard: string;
}
