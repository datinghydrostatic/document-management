import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatSort, MatPaginator } from '@angular/material';
import { SharedService } from '../services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {

  displayedColumns: string[] = ['clientId', 'contractId',
    'accountCommonName', 'billingStatus', 'effectiveDate', 'actions'
  ];
  tableData = [];
  dataSource = new MatTableDataSource(this.tableData);
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(public sharedService: SharedService, public router: Router) { }

  ngOnInit() {
  }

  onSearchDataOut(data) {
    this.tableData = data;
    this.dataSource = new MatTableDataSource(this.tableData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  rowClicked(row) {
    this.sharedService.searchResultSelectedRow = row;
    this.sharedService.onSearchResultRowSelect.next(row);
    this.router.navigateByUrl('/tabpage');
  }

  onReset() {
    this.tableData = [];
    this.dataSource = new MatTableDataSource([]);
  }

}
