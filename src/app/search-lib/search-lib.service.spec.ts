import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { SearchLibService } from './search-lib.service';
import { MatSnackBarModule } from '@angular/material';


describe('SearchLibService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, MatSnackBarModule],
    providers: [SearchLibService]
  }));

  it('should be created', () => {
    const service: SearchLibService = TestBed.get(SearchLibService);
    expect(service).toBeTruthy();
  });

  it('should have getSearchData function', () => {
    const service: SearchLibService = TestBed.get(SearchLibService);
    expect(service.getSearchData).toBeTruthy();
  });

});
