import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ClientSearch } from '../model';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class SearchLibService {
    isLoading = false;
    headers = new HttpHeaders({ Authorization: null });

    constructor(private http: HttpClient, public snackBar: MatSnackBar) { }

    getSearchData(data: any) {
        const url = 'api/searchResultData';
        return this.http.get<ClientSearch>(url, data)
            .pipe(
                catchError(this.handleError.bind(this)) // then handle the error
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        this.isLoading = false;
        this.snackBar.open('Failed.' + error.status + error.message, 'Clear',
            {
                duration: 152000,
                horizontalPosition: 'end',
                verticalPosition: 'top',
                panelClass: ['red-snackbar']
            });
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }

    makeIntentionalError() {
        return this.http.get('not/a/real/url')
            .pipe(
                catchError(this.handleError)
            );
    }

}
