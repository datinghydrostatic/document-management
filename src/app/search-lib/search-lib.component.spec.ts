import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { SearchLibComponent } from './search-lib.component';
import { SearchLibModule } from './search-lib.module';
import { MatSnackBarModule } from '@angular/material';

describe('SearchLibComponent', () => {
  let component: SearchLibComponent;
  let fixture: ComponentFixture<SearchLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SearchLibModule, MatSnackBarModule
      ],
      declarations: []
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
