import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ClientDocument } from 'src/app/model';
import { ApiService } from 'src/app/services/api.service';
import { error } from 'util';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class Tab2FormComponent implements OnInit {

  theForm: FormGroup;
  checked = false;
  upateableFields = {
    manualUpdateIndicator: true,
    billingGroupingName: true
  };
  constructor(public dialogRef: MatDialogRef<Tab2FormComponent>, @Inject(MAT_DIALOG_DATA) public data: ClientDocument,
    public apiService: ApiService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.theForm = new FormGroup({
      manualUpdateIndicator: new FormControl(null, Validators.required),
      billingGroupingName: new FormControl(null, Validators.required),
      originalEffectiveDate: new FormControl(null, Validators.required),
    });
    if (this.data) {
      this.theForm.patchValue(this.data);
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSaveAndClose() {
    if (this.data) {
      this.apiService.updateTab2Row(this.theForm.value)
        .subscribe(res => {
          this.snackBar.open('Your document has been updated.', 'ok',
            {
              duration: 2000,
            });
        });
    } else {
      this.apiService.addTab2Row(this.theForm.value)
        .subscribe(res => {
          this.snackBar.open('New entry has been Added.', 'ok',
            {
              duration: 2000,
            });
        });
    }

    this.dialogRef.close(null);
  }

  onViewAndCompare() {
    this.dialogRef.close(this.getEditableValues());
  }

  getEditableValues() {
    const editableValues = {};
    Object.keys(this.upateableFields)
      .forEach(key => {
        const currentControl = this.theForm.controls[key];

        if (currentControl.dirty && this.upateableFields[key]) {
          editableValues[key] = currentControl.value;
        }
      });

    return editableValues;
  }

}
