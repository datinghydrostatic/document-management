import { async, TestBed, inject } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { OverlayContainer } from '@angular/cdk/overlay';
import { AppModule } from 'src/app/app.module';
import { Tab2FormComponent } from './form.component';

describe('Tab2FormComponent', () => {
  let dialog: MatDialog;
  let overlayContainer: OverlayContainer;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        AppModule,
        MatDialogModule,
        NoopAnimationsModule
      ]
    });

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [Tab2FormComponent]
      }
    });

    TestBed.compileComponents();
  }));

  beforeEach(inject([MatDialog, OverlayContainer],
    (d: MatDialog, oc: OverlayContainer) => {
      dialog = d;
      overlayContainer = oc;
    })
  );

  afterEach(() => {
    overlayContainer.ngOnDestroy();
  });

  it('should create', () => {
    const dialogRef = dialog.open(Tab2FormComponent);
    // verify
    expect(dialogRef.componentInstance instanceof Tab2FormComponent).toBeTruthy();
  });

  it('should open a dialog with a component', () => {
    const dialogRef = dialog.open(Tab2FormComponent, {
      data: { param: '1' }
    });

    // verify
    expect(dialogRef.componentInstance instanceof Tab2FormComponent).toBe(true);
  });
});

