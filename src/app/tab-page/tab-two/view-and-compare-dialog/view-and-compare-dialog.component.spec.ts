import { async, TestBed, inject } from '@angular/core/testing';
import { ViewAndCompareDialogComponent } from './view-and-compare-dialog.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { OverlayContainer } from '@angular/cdk/overlay';
import { AppModule } from 'src/app/app.module';

describe('ViewAndCompareDialogComponent', () => {
  let dialog: MatDialog;
  let overlayContainer: OverlayContainer;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        AppModule,
        MatDialogModule,
        NoopAnimationsModule
      ]
    });

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [ViewAndCompareDialogComponent]
      }
    });

    TestBed.compileComponents();
  }));

  beforeEach(inject([MatDialog, OverlayContainer],
    (d: MatDialog, oc: OverlayContainer) => {
      dialog = d;
      overlayContainer = oc;
    })
  );

  afterEach(() => {
    overlayContainer.ngOnDestroy();
  });

  it('should create', () => {
    const dialogRef = dialog.open(ViewAndCompareDialogComponent);
    // verify
    expect(dialogRef.componentInstance instanceof ViewAndCompareDialogComponent).toBeTruthy();
  });

  it('should open a dialog with a ViewAndCompareDialogComponent', () => {
    const dialogRef = dialog.open(ViewAndCompareDialogComponent, {
      data: { param: '1' }
    });

    // verify
    expect(dialogRef.componentInstance instanceof ViewAndCompareDialogComponent).toBe(true);
  });
});


