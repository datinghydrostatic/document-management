import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { Tab2FormComponent } from '../form/form.component';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-view-and-compare-dialog',
  templateUrl: './view-and-compare-dialog.component.html',
  styleUrls: ['./view-and-compare-dialog.component.scss']
})
export class ViewAndCompareDialogComponent implements OnInit {
  displayedColumns: string[] = ['select', 'membershipGroupingId', 'field', 'oldValue', 'newValue'];
  tableData = [];
  dataSource = new MatTableDataSource<any>(this.tableData);
  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  constructor(public dialogRef: MatDialogRef<Tab2FormComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data) {
      this.dataSource = new MatTableDataSource<any>(this.data);
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSaveAndClose() {
    this.dialogRef.close(this.selection.selected);
  }

}
