import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabTwoComponent } from './tab-two.component';
import { AppModule } from 'src/app/app.module';

describe('TabTwoComponent', () => {
  let component: TabTwoComponent;
  let fixture: ComponentFixture<TabTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule
      ],
      declarations: []
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
