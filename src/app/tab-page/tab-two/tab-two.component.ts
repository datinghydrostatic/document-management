import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { SharedService } from 'src/app/services/shared.service';
import { ApiService } from 'src/app/services/api.service';
import { Tab2FormComponent } from './form/form.component';
import { ViewAndCompareDialogComponent } from './view-and-compare-dialog/view-and-compare-dialog.component';

class CompareTable {
  membershipGroupingId: number;
  field: string;
  oldValue: string;
  newValue: string;
}

@Component({
  selector: 'app-tab-two',
  templateUrl: './tab-two.component.html',
  styleUrls: ['./tab-two.component.scss']
})
export class TabTwoComponent implements OnInit {
  isLoading = false;
  tableData = [];
  displayedColumns: string[] = ['manualUpdateIndicator', 'membershipGroupingId',
    'billingGroupingName', 'originalEffectiveDate'];
  dataSource = new MatTableDataSource(this.tableData);
  constructor(public snackBar: MatSnackBar, public dialog: MatDialog,
    public sharedService: SharedService, public apiService: ApiService) { }
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;


  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.sharedService.onTab1RowSelect.subscribe(row => {
      if (this.sharedService.tab1SelectedRow) {
        this.loadData();
      }
    });

  }

  loadData() {
    this.isLoading = true;
    this.apiService.getTab2Data(this.sharedService.tab1SelectedRow.contractId)
      .subscribe(res => {
        this.isLoading = false;
        this.tableData = res;
        this.dataSource = new MatTableDataSource(this.tableData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.sharedService.tab2SelectedRow = res[0];
      });
  }

  onRowClicked(row) {
    this.sharedService.tab2SelectedRow = row;
  }

  onEdit() {
    const dialogRef = this.dialog.open(Tab2FormComponent, {
      data: this.sharedService.tab2SelectedRow
    });
    const compareTableData: CompareTable[] = [];
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        Object.keys(result)
          .forEach(key => {
            for (const row of this.tableData) {
              const compareTableRow = new CompareTable();
              compareTableRow.membershipGroupingId = row.membershipGroupingId;
              compareTableRow.field = key;
              compareTableRow.oldValue = row[key];
              compareTableRow.newValue = result[key];
              compareTableData.push(compareTableRow);
            }
          });
        const compareDialogRef = this.dialog.open(ViewAndCompareDialogComponent, {
          data: compareTableData,
          width: '800px'
        });
        compareDialogRef.afterClosed().subscribe(compResult => {
          if (compResult) {
            this.apiService.updateTab2Rows(compResult).subscribe(res => {
              this.snackBar.open('Updated successfully', 'ok', {
                duration: 2000,
              });
            });
          }
        });
      }
    }
    );
  }

  onAdd() {
    const dialogRef = this.dialog.open(Tab2FormComponent);
    dialogRef.afterClosed().subscribe(result => {
    });
  }


  onDelete() {
    const snackBarRef = this.snackBar.open('Are you sure to delete file', 'Yes', { duration: 15000 });
    snackBarRef.afterDismissed().subscribe(info => {
      if (info.dismissedByAction === true) {
        this.apiService.deleteTab1Row(this.sharedService.tab1SelectedRow)
          .subscribe(res => {
            this.loadData();
            this.snackBar.open('Your file has been deleted', '', {
              duration: 2000,
            });
          });
      }
    });
  }

}
