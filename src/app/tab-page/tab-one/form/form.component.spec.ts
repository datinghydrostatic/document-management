import { async, TestBed, inject } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { OverlayContainer } from '@angular/cdk/overlay';
import { FormComponent } from './form.component';
import { AppModule } from 'src/app/app.module';

describe('Tab1FormComponent', () => {
  let dialog: MatDialog;
  let overlayContainer: OverlayContainer;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        AppModule,
        MatDialogModule,
        NoopAnimationsModule
      ]
    });

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [FormComponent]
      }
    });

    TestBed.compileComponents();
  }));

  beforeEach(inject([MatDialog, OverlayContainer],
    (d: MatDialog, oc: OverlayContainer) => {
      dialog = d;
      overlayContainer = oc;
    })

  );

  afterEach(() => {
    overlayContainer.ngOnDestroy();
  });

  it('should create', () => {
    const dialogRef = dialog.open(FormComponent, {
      data: { param: '1' }
    });

    // verify
    expect(dialogRef.componentInstance instanceof FormComponent).toBeTruthy();
  });

  it('should open a dialog with a component', () => {
    const dialogRef = dialog.open(FormComponent, {
      data: { param: '1' }
    });

    // verify
    expect(dialogRef.componentInstance instanceof FormComponent).toBe(true);
  });

});

