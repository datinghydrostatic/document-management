import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ClientDocument } from 'src/app/model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  theForm: FormGroup;
  constructor(public snackBar: MatSnackBar, public dialogRef: MatDialogRef<FormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ClientDocument,
    public apiService: ApiService) { }

  ngOnInit() {

    this.theForm = new FormGroup({
      salesAccountCommonName: new FormControl(null, Validators.required),
      effectiveBillingStatus: new FormControl(null, Validators.required),
      billingPendedIndicator: new FormControl(null, Validators.required),
      eligibilityStatus: new FormControl(null, Validators.required),
      currentAnniversaryDate: new FormControl(null, Validators.required),
      effectiveDateCreateDate: new FormControl(null, Validators.required),
      // nextAnniversaryDate: new FormControl(null, Validators.required),
      // status: new FormControl(null, Validators.required),
      // billingStatus: new FormControl(null, Validators.required),
      // renewalLetterAdvanceMonths: new FormControl(null, Validators.required),
      // renewalReportDate: new FormControl(null, Validators.required),
      // delayPaymentPeriodForBenefit: new FormControl(null, Validators.required),
      // limitBenefitByFutureTermDate: new FormControl(null, Validators.required),

    });
    if (this.data) {
      this.theForm.patchValue(this.data);
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSaveAndClose() {


    if (this.data) {
      this.apiService.updateTab1Row(this.theForm.value)
        .subscribe(res => {
          this.snackBar.open('Your document has been updated.', 'ok', { duration: 2000 });
        });
    } else {
      this.apiService.addTab1Row(this.theForm.value)
        .subscribe(res => {
          this.snackBar.open('New entry has been Added.', 'ok', { duration: 2000 });
        });
    }

    this.dialogRef.close();
  }

}
