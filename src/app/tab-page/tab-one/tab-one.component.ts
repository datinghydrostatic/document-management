import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatPaginator, MatSnackBar } from '@angular/material';
import { FormComponent } from './form/form.component';
import { SharedService } from 'src/app/services/shared.service';
import { ApiService } from 'src/app/services/api.service';
import { CommentDialogComponent } from './comment-dialog/comment-dialog.component';

export interface CommentsData {
  date: string;
  comment: string;
}

const COMMENTS: CommentsData[] = [
  { date: '######', comment: 'Angular is running in the development mode.' },
  { date: '######', comment: 'Angular is running in the development mode. ' },
  { date: '######', comment: 'Angular is running in the development mode. ' }
];

@Component({
  selector: 'app-tab-one',
  templateUrl: './tab-one.component.html',
  styleUrls: ['./tab-one.component.scss']
})

export class TabOneComponent implements OnInit {
  isLoading = false;
  tableData = [];
  displayedColumns: string[] = ['status', 'contractId', 'salesAccountCommonName', 'address', 'effectiveDateCreateDate'];
  dataSource = new MatTableDataSource(this.tableData);
  constructor(public snackBar: MatSnackBar, public dialog: MatDialog,
    public sharedService: SharedService, public apiService: ApiService) { }
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  comments = new MatTableDataSource(COMMENTS);
  displayedCommentsColumns: string[] = ['date', 'comment'];

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.sharedService.onSearchResultRowSelect.subscribe(row => {
      if (row) {
        this.loadData(row);
      }
    });


  }

  loadData(row) {
    this.isLoading = true;
    this.apiService.getTab1Data(row.clientId)
      .subscribe(res => {
        this.isLoading = false;
        this.tableData = res;
        this.dataSource = new MatTableDataSource(this.tableData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.onRowClicked(res[0]);
      });
  }

  onRowClicked(row) {
    this.sharedService.tab1SelectedRow = row;
    this.sharedService.onTab1RowSelect.next(row);
  }

  onEdit() {
    this.dialog.open(FormComponent, {
      data: this.sharedService.tab1SelectedRow
    });
  }

  onAdd() {
    const dialogRef = this.dialog.open(FormComponent);
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  onAddComment() {
    const dialogRef = this.dialog.open(CommentDialogComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.apiService.addComment(result)
          .subscribe(res => {
            this.snackBar.open('Your comment has been Added', 'close', {
              duration: 2000,
            });
          });
      }
    });
  }

  onDelete() {
    const snackBarRef = this.snackBar.open('Are you sure to delete file', 'Yes', { duration: 15000 });
    snackBarRef.afterDismissed().subscribe(info => {
      if (info.dismissedByAction === true) {
    this.apiService.deleteTab1Row(this.sharedService.tab1SelectedRow)
      .subscribe(res => {
        this.loadData(this.sharedService.searchResultSelectedRow);
        this.snackBar.open('Your file has been deleted', 'close', {
          duration: 2000,
        });
      });
      }
    });
  }

}
